# SMARTEX WEB CHALLENGE

WEB Dev Challenge for SINF participants that are visiting SMARTEX Porto office at 30/10/2023.

## Goals

Build a software tool capable of automatize the process of labeling images.

## Context

To improve the performance of model inference, having a good dataset is very important for smartex.

So with this challenge we want you to create an web tool in order to help the data/ml engineers to label images.

What is labeling? In this context, the user associates a bunch of labels to the a set of images that will be used to train the models.

## Repository Structure

### server folder

Simple server code which connects to a postgres database.

We are using `Prisma` as the ORM.

Contains the base code for the Backend Challenge

### labelling

Frontend app created with [create react app](https://react.dev/learn/start-a-new-react-project) and using [MUI](https://mui.com/).

Contains the base code for the Frontend Challenge.

## Installation

### Pre-requirements

- Docker

### Steps

```sh
docker compose up --build
```

**When running for the first time**

When you run for the first time or you unmount all the volumes associated to these services, you will need to create the database, run the migrations and run the seed file.

First, get inside the container that is running the server

```sh
docker exec -it smartex_backend /bin/sh
```

Second, create the database:

```sh
npx prisma db push
```

Third, run the seed file:

```sh
npx prisma db seed
```

Now, your [frontend page](http://localhost:3000) might show

![Alt text](image.png)

## Data Structures

### Frame

A `Frame` is an object that represents an image with the associated metadata.

#### SQL Schema

```sql
CREATE TABLE "frames" (
    "id" UUID NOT NULL DEFAULT gen_random_uuid(),
    "key" VARCHAR NOT NULL,
    "creation_timestamp" TIMESTAMPTZ(3) NOT NULL,
    "light" "light_type_enum" NOT NULL,
    "position" "position_type_enum" NOT NULL,
    "camera" INTEGER,
    "url" VARCHAR,

    CONSTRAINT "frames_pkey" PRIMARY KEY ("id")
);
```

### Sample

A `Sample` is a set of `Frames` of the same camera for the same request. To simplify the problem we consider max 2 frames per sample both taken with IR light. For example:

Frame 1:

```json
{ "sample_id": 1, "position": "TOP", "light": "IR", "camera": 0 }
```

Frame 2:

```json
{ "sample_id": 1, "position": "FRONT", "light": "IR", "camera": 0 }
```

### Label

A `Label` is a symbol. It can be connected to other pre-existing label. So, these labels have a tree structure. For example:

Label 1:

```json
{
  "id": 1,
  "name": "Defect",
  "parent_id": null
}
```

Label 2:

```json
{
  "id": 2,
  "name": "Horizontal",
  "parent_id": 1
}
```

#### SQL Schema

```sql
CREATE TABLE "labels" (
    "id" INTEGER NOT NULL,
    "name" VARCHAR(50) NOT NULL,
    "parent_id" INTEGER,
);
```


## License

[MIT](https://choosealicense.com/licenses/mit/)
