/*
  Warnings:

  - Made the column `camera` on table `frames` required. This step will fail if there are existing NULL values in that column.
  - Made the column `url` on table `frames` required. This step will fail if there are existing NULL values in that column.

*/
-- AlterTable
ALTER TABLE "frames" ALTER COLUMN "camera" SET NOT NULL,
ALTER COLUMN "url" SET NOT NULL;
