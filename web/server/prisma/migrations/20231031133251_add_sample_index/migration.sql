/*
  Warnings:

  - Added the required column `sample_index` to the `frames` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "frames" ADD COLUMN     "sample_index" INTEGER NOT NULL;
