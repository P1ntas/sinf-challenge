import { Router } from 'express';
import { PrismaClient } from '@prisma/client';
import { requestLabels } from '../controller/labels';

const router = Router();

const findLabels = (prisma: PrismaClient) => router.get('/', requestLabels(prisma));

export default (prisma: PrismaClient) => router.use([findLabels(prisma)]);
