import { Router } from 'express';
import { validate } from 'express-validation';
import { requestFrames, requestFramesValidation } from '../controller/frames';
import { PrismaClient } from '@prisma/client';

const router = Router();

const findFrames = (prisma: PrismaClient) => router.get('/', requestFrames(prisma));

export default (prisma: PrismaClient) => router.use([findFrames(prisma)]);
