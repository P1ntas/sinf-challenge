import { Router } from 'express';
import { PrismaClient } from '@prisma/client';
import frames from './frames';
import labels from './labels';

export default (prisma: PrismaClient) => {
  const routes = Router();
  routes.get('/', (_req, res) => res.status(200).json({ message: 'Welcome to the base' }));
  routes.use('/frames', frames(prisma));
  routes.use('/labels', labels(prisma));
  return routes;
};
