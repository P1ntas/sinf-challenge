import { $Enums, PrismaClient } from '@prisma/client';
import { Response, Request, RequestHandler } from 'express';
import Joi from 'joi';

interface Frame {
  id: string;
  key: string;
  creation_timestamp: Date;
  light: $Enums.light_type_enum;
  position: $Enums.position_type_enum;
  camera: number;
  url: string;
  sample_index: number;
}
export const requestFramesValidation = {
  query: Joi.object({ nSamples: Joi.number().optional() }),
};

export const requestFrames =
  (prisma: PrismaClient): RequestHandler =>
  async (req: Request, res: Response) => {
    const {
      query: { n },
    } = req;
    const frames = await prisma.frames.findMany({
      take: n ? parseInt(n as string) * 2 : undefined,
    });

    const framesPerCameraPerIndex = frames.reduce<{
      [camera: number]: { [sampleIndex: number]: Frame[] };
    }>((samplesPerCamera, frame) => {
      const { camera, sample_index } = frame;
      samplesPerCamera[camera] ??= {};
      samplesPerCamera[camera][sample_index] ??= [];
      samplesPerCamera[camera][sample_index].push(frame);
      return samplesPerCamera;
    }, {});

    res.send(framesPerCameraPerIndex);
  };
