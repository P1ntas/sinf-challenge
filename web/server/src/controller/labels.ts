import { $Enums, PrismaClient } from '@prisma/client';
import { Response, Request, RequestHandler } from 'express';
import Joi from 'joi';

export const requestLabels =
  (prisma: PrismaClient): RequestHandler =>
  async (_: Request, res: Response) => {
    const labels = await prisma.labels.findMany();

    res.send(labels);
  };
