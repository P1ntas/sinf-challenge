import { URL } from 'node:url';

import { cleanEnv, str, port, makeValidator } from 'envalid';
import dotenv from 'dotenv';

dotenv.config();

const origins = makeValidator<(string | RegExp)[]>((x: string) => {
  let originURLs: string[];
  try {
    originURLs = JSON.parse(x);
  } catch (error) {
    throw new Error(`Invalid urls: "${x}"`);
  }
  return originURLs.map((originURL, index) => {
    if (originURL.startsWith('regex://')) {
      /**
       * In GitLab CI variable we can not add `$`
       * because that conflict with unix based
       * variables.
       */
      if (!originURL.endsWith('$')) originURL += '$';
      const pattern = originURL.replace('regex://', '');
      return new RegExp(pattern);
    } else {
      try {
        const parseURL = new URL(originURL);
        if (!parseURL.origin || parseURL.origin === 'null') {
          throw new Error(`Invalid url at position [${index}]: "${originURL}"`);
        }
        return parseURL.origin;
      } catch (e) {
        throw new Error(`Invalid url at position [${index}]: "${originURL}"`);
      }
    }
  });
});

const env = cleanEnv(process.env, {
  NODE_ENV: str({
    choices: ['production', 'test', 'development'],
    default: 'development',
  }),
  PORT: port({ default: 3333 }),
  DATABASE_URL: str(),
  WHITELIST_ORIGINS: origins({ default: undefined }),
});

export { env };
