# SMARTEX FRONTEND CHALLENGE

Challenge for SINF participants that are visiting SMARTEX Porto office at 30/10/2023.

## Description

React application responsible for the UI of the labelling tool. Here the user can request frames and associate to each one a different set of samples.

## Already implemented

- Singleton Service that contains an axios instance pointing to the backend app.

## Goals

1. Allow the user to request labels from backend app
2. Allow the user to request frames from backend app
3. Show each `Sample` (set of 2 `Frames`).
4. Select labels to associate to the `Sample` being shown
5. Navigate per sample, per camera. For example: the user is at the sample a) that belongs to the camera `0`, and sample_index `0`. If the user (suggestion) selects a button to go to the next sample, the user would see the sample from camera `0` with the sample_index `1`.

## Installation

Follow the instructions on [README](../README)

## Observations

The current endpoints are already created in the backend:

- GET `/frames`
- GET `/labels`

The GET `/frames` return a JSON Object that already separate all samples per camera and per sample index .

```json
{
  "Camera": { "Sample Index": ["Frame A", "Frame B"] }
}
```

- Camera - values between [0,3]
- Sample Index - values between [0, n], where n is an integer > 0


The UI is up to you.

## License

[MIT](https://choosealicense.com/licenses/mit/)
