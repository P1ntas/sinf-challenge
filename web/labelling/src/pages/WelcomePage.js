import React, { useEffect, useMemo, useState } from "react";
import { getFrames } from "../endpoints/frames";
import { Box, CircularProgress, Container, Stack } from "@mui/material";

const WelcomePage = () => {
  const [frames, setFrames] = useState({});
  const [busy, setBusy] = useState(false);
  const [error, setError] = useState(null);

  const cameras = useMemo(() => Object.keys(frames), [frames]);
  const samplesPerCamera = useMemo(() => {
    return Math.max(
      ...cameras.map((cameraIndex) => Object.values(frames[cameraIndex]).length)
    );
  }, [cameras, frames]);

  useEffect(() => {
    setBusy(true);
    getFrames()
      .then(({ data }) => {
        setFrames(data);
      })
      .catch((error) => {
        setError(error);
      })
      .finally(() => {
        setBusy(false);
      });
  }, []);

  return (
    <Container>
      <h1> Number of Frames </h1>
      {busy ? (
        <Box sx={{ display: "flex" }}>
          <CircularProgress />
        </Box>
      ) : error ? (
        <p>ERROR</p>
      ) : (
        <Stack>
          <p>Number Of Cameras: {cameras.length}</p>
          <p>Number Of Samples Per Camera: {samplesPerCamera}</p>
        </Stack>
      )}
    </Container>
  );
};

export default WelcomePage;
