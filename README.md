# SINF Challenge

## Instructions 

### Development

Fork and do your work in your own repository. 

### Delivery 

Send an e-mail to daniel.gomes@smartex.ai with the public repository or add @daniel.gomes5 to the repository, with run instructions if needed. 

Optional: readme declaring every feature implemented. 

## Disclaimer

If you need help with anything, discuss code, build problems, etc etc... send an e-mail to daniel.gomes@smartex.ai, I promise I'll answer. 

Good luck! 
